Ce dépôt contient le code source (i.e. les pages Web) du site
[https://balabox.gitlab.io/balabox/](https://balabox.gitlab.io/balabox/).

---

Les pages sont écrites au format Markdown. Les pages statiques sont
générées à l'aide de l'outil [Hugo], des GitLab Pages et de [GitLab
CI][ci], en suivant les étapes définies dans
[`.gitlab-ci.yml`](.gitlab-ci.yml).

Pour en savoir plus sur les GitLab Pages, vous pouvez vous référer à
[https://pages.gitlab.io](https://pages.gitlab.io) et à la
documentation officielle
[https://docs.gitlab.com/ce/user/project/pages/](https://docs.gitlab.com/ce/user/project/pages/).

---

## Installation locale

Pour visualiser ce projet localement sur votre machine, vous devez suivre les étapes suivantes:

1. Clonez ou téléchargez le projet
1. [Installez][] Hugo
1. Visualisez le site Web en exécutant la commande: `hugo server`
1. Ajoutez du contenu
1. Générez le site Web localement via la commande: `hugo` (optionel)


[ci]: https://about.gitlab.com/gitlab-ci/
[hugo]: https://gohugo.io
[installez]: https://gohugo.io/overview/installing/
