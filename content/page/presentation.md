
[Balabox](https://balabox.gitlab.io/balabox) est une solution nomade
de création, de partage et de diffusion de contenus numériques à
destination de l'enseignement basée sur la
[MoodleBox](https://moodlebox.net/fr/). 

MoodleBox offre un environnement d’apprentissage Moodle complet,
permettant de fournir des activités d’apprentissage et partager des
fichiers. MoodleBox fonctionne sur les modèles de Raspberry Pi 3A+,
3B, 3B+ et 4B.

MoodleBox peut fonctionner sans connexion Internet. Il crée un réseau
Wi-Fi pour permettre à des terminaux mobiles (e.g. téléphones,
tablettes) d'accéder à des contenus numériques, et d'en partager.  Si
la MoodleBox est connectée à un réseau Ethernet fournissant un accès à
Internet, il est possible d'accéder depuis un terminal mobile à
d’autres sites web ou services sur Internet.

 

Balabox étend MoodleBox en intégrant des services et fonctionnalités
supplémentaires très utiles pour la gestion de classe et le partage de
contenus numériques. Ces services et fonctionnalités supplémentaires
sont les suivants:

* identification des élèves et des groupes d'élèves (projet [identification](https://gitlab.com/balabox/identification)),
* création et transfert de contenus numériques audio, photo et vidéo (projet [recorder](https://gitlab.com/balabox/recorder)),
* partage simplifié de fichiers entre l'enseignant et les élèves (projet [partage](https://gitlab.com/balabox/partage)),
* suivi de l'activité d'une classe à travers un tableau de bord en temps réel (projet [board-app](https://gitlab.com/balabox/board-app)),
* outils développés par [ladigitale.dev](https://ladigitale.dev) (projet [ladigitale](https://gitlab.com/balabox/ladigitale)),
* suivi d'activités sportives (projet [sport-app](https://gitlab.com/balabox/sport-app)).

L’usage est simple: l’enseignant dépose sur le boîtier tout le matériel pédagogique depuis son ordinateur, sa tablette ou son smartphone. Les utilisateurs n’ont ensuite qu’à connecter leur appareil mobile au réseau local généré par la BalaBox pour accéder à du contenu et en déposer. Ceci permet aux enseignants de restreindre les échanges des étudiants au seul réseau du boîtier, sans que les données ne soient diffusées sur Internet, un atout indéniable dans la protection des données personnelles.