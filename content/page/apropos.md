---
title: À Propos
menu: "main"
weight: 40
---
Le projet BalaBox est un projet initié par le Collège Numérique 56
pour répondre aux besoins de solutions numériques pour des
enseignements dispensés dans les collèges du département du Morbihan.

Le projet BalaBox est développé par les étudiants de seconde année de
BUT Informatique de l'IUT de Vannes.

## Collège Numérique 56
Il s'agit d'une opération menée par le Département du Morbihan ayant
pour objectif de développer l’usage des équipements numériques à des
fins pédagogiques (exemples: imprimantes 3D, tablettes, drones...).


![logo collège numérique 56](/balabox/images/logo_cn56.png) ![cg56](/balabox/images/logo_cd56.png)

Site Web: [https://www.collegenumerique56.fr/](https://www.collegenumerique56.fr/)


## Département Informatique de l'IUT de Vannes
Dans le cadre des situations d'apprentissage et d'évaluation (S.A.É.)
de seconde année du BUT Informatique, les étudiants sont amenés à
effectuer des projets de développement. À ce titre, ils participent au
développement du projet BalaBox.

![iut](/balabox/images/logo_iut_vannes.jpg)

Site Web: [https://www.iutvannes.fr/b-u-t-informatique/](https://www.iutvannes.fr/b-u-t-informatique/)
